from locust import between, task, TaskSet, HttpUser
import json, os

user_name = os.environ.get('USER_NAME')
user_password = os.environ.get('USER_PASSWORD')

class PublicSection(TaskSet):
    @task
    def get_home_public(self):
        path = '/'
        response = self.client.get(path)
        print("GET %s %s" % (path, response.status_code))

class AuthenticatedSection(TaskSet):
    cookies = None

    def login(self):
        path = "/login"
        data = {"userName": user_name, "password": user_password, "tahunanggaran": "2021"}
        response = self.client.post("/login", data=json.dumps(data), headers={"accept": "application/json", "content-type": "application/json"})
        if response:
            print("POST %s %s" % (path, response.status_code))
            self.cookies = response.cookies
        pass

    def on_start(self):
        self.login()

    @task
    def get_home(self):
        if self.cookies:
            path = "/home"
            response = self.client.get(path, headers={"accept": "application/json"})
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def get_notification(self):
        if self.cookies:
            path = "/get-notification"
            response = self.client.get(path, headers={"accept": "application/json"})
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def get_pagu_skpd(self):
        if self.cookies:
            path = "/chart/pagu-skpd"
            response = self.client.get(path, headers={"accept": "application/json"})
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def get_dashboard_skpd_page(self):
        if self.cookies:
            path = "/dashboard/skpd"
            response = self.client.get(path)
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def get_spd_create_page(self):
        if self.cookies:
            path = "/spd"
            response = self.client.get(path)
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def get_spd_otorisasi_page(self):
        if self.cookies:
            path = "/otorisasi-spd"
            response = self.client.get(path)
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def get_tuppkd_otorisasi_page(self):
        if self.cookies:
            path = "/otorisasi-tu-ppkd"
            response = self.client.get(path)
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def get_spm_verifikasi_page(self):
        if self.cookies:
            path = "/spm/verifikasi"
            response = self.client.get(path)
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def get_periode_spd(self):
        if self.cookies:
            path = "/data/periode-spd"
            response = self.client.get(path, headers={"accept": "application/json"})
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def get_spd_penanda_tangan(self):
        if self.cookies:
            path = "/data/spd/penanda-tangan"
            response = self.client.get(path, headers={"accept": "application/json"})
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def get_skpd_all(self):
        if self.cookies:
            path = "/data/skpd/all"
            response = self.client.get(path, headers={"accept": "application/json"})
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def get_spd_data(self):
        if self.cookies:
            path = "/data/kegiatanbl/633/operasiModal/1"
            response = self.client.get(path, headers={"accept": "application/json"})
            print("GET %s %s" % (path, response.status_code))

            path = "/data/kegiatanbl/633/all/0"
            response = self.client.get(path, headers={"accept": "application/json"})
            print("GET %s %s" % (path, response.status_code))

            path = "/data/spd/633/all/0"
            response = self.client.get(path, headers={"accept": "application/json"})
            print("GET %s %s" % (path, response.status_code))

            path = "/data/spd/633/all/1"
            response = self.client.get(path, headers={"accept": "application/json"})
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def get_spd_print(self):
        if self.cookies:
            path = "/print/spd/552"
            response = self.client.get(path)
            print("GET %s %s" % (path, response.status_code))
        pass

    @task
    def stop(self):
        self.interrupt()


class WebUser(HttpUser):
    wait_time = between(0.5, 5)
    tasks = {PublicSection: 1, AuthenticatedSection: 5}